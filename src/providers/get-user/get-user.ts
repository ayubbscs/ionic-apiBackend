import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
let apiUrl = 'http://localhost/example/';

/*
  Generated class for the GetUserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GetUserProvider {

  constructor(public http: Http) {
    console.log('Hello GetUserProvider Provider');
  }

  getLocalData(){
    return this.http.get('http://localhost/example/getUser.php').map(res => res.json());
      // this.person = data.student[0].name;
      // console.log(this.person);
      // return this.person;    
      // console.log(this.person);
      // console.log(data);
    
  }

  postData(credentials) {
    //console.log(credentials);
    //console.log(type);
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      // let body = JSON.stringify(credentials);
      // console.log(body);
      this.http.post(apiUrl + 'insertUser.php', JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.text());
        }, (err) => {
          reject(err);
        });
    });
  }

}