import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import {Http, Response} from '@angular/http';
import { GetUserProvider } from '../../providers/get-user/get-user';
import 'rxjs/add/operator/map';
import { NgForm } from '@angular/forms';
import { SuccessPage } from '../success/success';
import { SignupPage } from '../signup/signup';
import { AlertController } from 'ionic-angular';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  names: any = null;
  password: any;

  name: any;
  pass: any;
  user: any;

  constructor(private alertCtrl: AlertController,public navCtrl: NavController, public GetUserProvider: GetUserProvider,  public http: Http, public loadingCtrl: LoadingController) {
    //this.getData();
  }

  // getData(){
  //   this.http.get('localhost/example/getUser.php').map(res => res.json()).subscribe(data => {
  //     console.log(data);

  // });
  // }

   getData(name,pass){
    this.GetUserProvider.getLocalData().subscribe(
    data => {
      this.names=[];
      this.password=[];
      for (var _i = 0; _i < 2; _i++) {
        this.names[_i] = data[_i].name;
        this.password[_i] = data[_i].password;
        
        if(this.names[_i]==name && this.password[_i]==pass){
          this.user = this.names[_i];
          window.localStorage['token'];
          console.log(this.user);
          //this.GetUserProvider.login();
          // this.login(this.user);
        }
      }
      
      if(this.user != null){
        this.navCtrl.setRoot(SuccessPage);
      }else{
        this.presentAlert();
      }
    });
  }

  // login(user){
  //   this.http.post('localhost/example/getUser.php',this.user)
  //     .toPromise().then((response) => {
  //     window.localStorage['token'] = response.json().token;
  //     this.navCtrl.setRoot(SuccessPage)
  //   });
  // }
  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Authentication Error',
      subTitle: 'Username or Password is Incorrect',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  checkUser(form: NgForm) {
    this.name = form.value['username'];
    this.pass = form.value['password'];
    this.getData(this.name,this.pass);
  }

  signUP(){
    this.navCtrl.setRoot(SignupPage);
  }
}
