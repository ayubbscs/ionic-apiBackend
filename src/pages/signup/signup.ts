import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { GetUserProvider } from '../../providers/get-user/get-user';
import { HomePage } from '../home/home';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  responseData: any;
  name: any;
  email:any;
  password: any;
  uni: any; 
  info = {"username": "","email": "", "password": "","university": ""};
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public GetUserProvider: GetUserProvider) {
    //console.log(this.info);
  }  

  signUP(){
    //console.log(this.info);
    this.GetUserProvider.postData(this.info).then((result) => {
      
     this.responseData = result;
     console.log(this.responseData);
     localStorage.setItem('info', JSON.stringify(this.responseData));
     this.navCtrl.push(HomePage);
   }, (err) => {
     // Error log
   });

 }

}
