import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { GetUserProvider } from '../../providers/get-user/get-user';
import { NgForm } from '@angular/forms';
import {Http, Response} from '@angular/http';
/**
 * Generated class for the SuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-success',
  templateUrl: 'success.html',
})
export class SuccessPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //this.logOut();
  }

  logOut(){
    this.navCtrl.setRoot(HomePage);
  }

  

  
  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad SuccessPage');
  // }

}
