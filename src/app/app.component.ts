import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { SuccessPage } from '../pages/success/success';
import { GetUserProvider } from '../providers/get-user/get-user';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public GetUserProvider: GetUserProvider) {
    // platform.ready().then(() => {
    //   // Okay, so the platform is ready and our plugins are available.
    //   // Here you can do any higher level native things you might need.
    //   statusBar.styleDefault();
    //   splashScreen.hide();
    // });
    
    // this.GetUserProvider.login().then((IsLoggedIn) =>{
    //   if(IsLoggedIn){
    //     this.rootPage = SuccessPage;
    //   }else{
    //     this.rootPage = HomePage;
    //   }
    // });

  }
}

